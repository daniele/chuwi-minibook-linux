# chuwi-minibook-linux

Random notes for making a chuwi minibook (8100-Y) work with linux


## Tests with debian testing (bullseye)

- Installation:
    - The ``firmware-iwlwifi`` package must be made available to the installer, otherwise wifi will not be available.
    - The installer will not install ``cryptsetup-initramfs`` even if the installation is made with encrypted/LVM selection.
- Almost everything basically works out of the box. Caveats:
    - mmc1 (external micro-sd card reader) outputs errors to dmesg if there is no card inserted. Inserted cards seem to work well.
    - Touchscreen works, but active stylus doesn't. https://gitlab.com/AdyaAdya/goodix-touchscreen-linux-driver adds support for it, but i2c input seems to crash randomly with it.
    - closing the lid to suspend works randomly, sometimes it yields to device-powered-on-but-completely-unresponsive. Pushing the power button to suspend seems to work reliably. Opening the lid afterwards also works reliably.
    - powertop is unable to tune quite a lot of tunables. ``tlp`` works well and there are many less tunables left in a "bad" state with it.
    - the fan whine is noticeable, and there is no way to control the fan speed from linux
    - the powerbutton should work also as a fingerprint reader, according to the official drivers it should be made by Focaltech, the device is nower to be seen neither in ``lspci`` nor in ``lsusb``. Perhaps it's attached via i2c?
 


